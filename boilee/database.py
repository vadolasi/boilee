from sqlalchemy import (
    Boolean,
    Column,
    ForeignKey,
    Integer,
    LargeBinary,
    String,
    create_engine,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker

Base = declarative_base()


class Boilerplate(Base):
    __tablename__ = "boilerplates"

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    docopt_string = Column(String, nullable=False)
    inquirer_answers = Column(String, nullable=False)

    paths = relationship("Path")


class Path(Base):
    __tablename__ = "paths"

    id = Column(Integer, primary_key=True)
    boilerplate_id = Column(Integer, ForeignKey("boilerplates.id"), nullable=False)
    path = Column(String, nullable=False)
    content = Column(LargeBinary)
    is_directory = Column(Boolean, nullable=False)

    boilerplate = relationship("Boilerplate")


class Database:
    def __init__(self, directory):
        self.path = f"{directory}/database.sqlite"
        engine = create_engine(f"sqlite:///{self.path}")

        Base.metadata.create_all(engine)

        Session = sessionmaker(bind=engine)
        self.session = Session()

    def commit(self, line_object):
        self.session.add(line_object)
        self.session.commit()

    def add_boilerplate(self, **args):
        new_boilerplate = Boilerplate(**args)

        return new_boilerplate

    def add_path(self, **args):
        new_path = Path(**args)

        return new_path
