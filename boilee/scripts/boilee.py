import json
import os
import shutil
import sys
import tempfile

import click
import docopt
import emoji
import pyfiglet
import yaml
from gitignore_parser import parse_gitignore
from halo import Halo
from jinja2 import Template
from PyInquirer import Separator, prompt

from boilee.database import Database


def default_values(parameter, parameter_name):
    if parameter["input-type"] == "input":
        return {
            **parameter,
            **{
                "parameter-type": "option",
                "deafult": None,
                "only": "all",
                "message": parameter_name,
            },
        }
    elif parameter["input-type"] == "confirm":
        return {
            **parameter,
            **{"only": "all", "message": parameter_name, "deafult": False, "help": "",},
        }
    elif parameter["input-type"] in ["list", "rawlist"]:
        return {
            **parameter,
            **{
                "parameter-type": "option",
                "default": 1,
                "only": "all",
                "message": parameter_name,
                "help": "",
            },
        }
    elif parameter["input-type"] == "checkbox":
        parameter["choices"] = [
            {
                list(choice.keys())[0]: {
                    **{"checked": False, "type": "option"},
                    **list(choice.values())[0],
                }
            }
            for choice in parameter["choices"]
        ]
        return {
            **parameter,
            **{
                "parameter-type": "option",
                "only": "all",
                "message": parameter_name,
                "help": "",
            },
        }


@click.group()
def cli():
    pass


@cli.command()
@click.argument("src")
@click.argument("boilerplate_name")
def make(src, boilerplate_name):
    click.echo(pyfiglet.figlet_format("boilee", font="slant"))

    run_dir = os.getcwd()
    os.chdir(src)

    with Halo(text="Processing parameters", spinner="dots"):

        try:
            with open(f".boilee/parameters.yml", "r") as parameters_file:
                parameters = yaml.load(parameters_file.read(), Loader=yaml.FullLoader)

                parameters = {
                    key: default_values(value, key)
                    for (key, value) in parameters.items()
                }

        except FileNotFoundError:
            sys.exit(
                emoji.emojize(
                    ":broken_heart: :boom:\033[1m parameters.yml not found \033[0m:boom: :broken_heart:\n",
                    use_aliases=True,
                )
            )

        docopt_usage = "    boilee run <src> <dst>"
        docopt_options = "    -h --help     Show this screen.\n"

        inquirer_answers = []

        for key, value in parameters.items():

            if value["input-type"] == "input":
                if value["only"] in ["all", "parameter"]:
                    if value["type_parameter"] == "argument":
                        docopt_usage = f"{docopt_usage} {f'<{key}>' if value['required'] else f'[<{key}>]'}"
                    else:
                        docopt_usage = f"{docopt_usage} {f'--{key}=<{key}>' if value['required'] else f'[--{key}=<{key}>]'}"
                        docopt_options = (
                            f"{docopt_options}    --{key}=<{key}>    {value['help']}\n"
                        )

                if value["only"] in ["all", "input"]:
                    inquirer_answers.append(
                        {
                            "type": value["input-type"],
                            "name": key,
                            "message": value["message"],
                            "default": value["default"],
                        }
                    )

            elif value["input-type"] in ["list", "rawlist"]:
                if value["only"] in ["all", "parameter"]:
                    choices_list = [
                        f"{choice}\n"
                        for choice in value["choices"]
                        if not "separator" in choice
                    ]

                    choices = "".join(choices_list).replace("\n", "|")[:-1]
                    docopt_usage = f"{docopt_usage} {f'--{key}=({choices})' if value['required'] else f'[--{key}=({choices})]'}"
                    docopt_options = (
                        f"{docopt_options}    --{key}={key}    {value['help']}\n"
                    )

                if value["only"] in ["all", "input"]:
                    inquirer_answers.append(
                        {
                            "type": value["input-type"],
                            "name": key,
                            "choices": [
                                choice
                                if not "separator" in choice
                                else Separator(choice).__dict__
                                for choice in value["choices"]
                            ],
                            "default": value["default"],
                        }
                    )

            elif value["input-type"] == "confirm":
                if value["only"] in ["all", "parameter"]:
                    docopt_usage = f"{docopt_usage} --{key}"
                    docopt_options = f"{docopt_options}    --{key}    {value['help']}\n"

                if value["only"] in ["all", "input"]:
                    inquirer_answers.append(
                        {"type": "confirm", "name": key, "default": value["default"]}
                    )

            elif value["input-type"] == "checkbox":
                if value["only"] in ["all", "parameter"]:
                    # TODO: Add checkbox for docopt parameters
                    pass

                if value["only"] in ["all", "input"]:
                    inquirer_answers.append(
                        {
                            "type": "checkbox",
                            "name": key,
                            "choices": [
                                list(choice.values())[0]
                                if list(choice.values())[0]["type"] != "separator"
                                and list(choice.values())[0]["type"] == "option"
                                else Separator(list(choice.items())[0]).__dict__
                                if not "disabled" in choice
                                else {
                                    "name": key,
                                    "disabled": list(choice.values())[0]["disabled"],
                                }
                                for choice in value["choices"]
                            ],
                        }
                    )

    docopt_string = f"Usage:\n{docopt_usage}\n\nOptions:\n{docopt_options}"

    spinner = Halo(text="Processing files", spinner="dots")
    spinner.start()

    with tempfile.TemporaryDirectory() as temp_dir:
        database = Database(temp_dir)
        boilerplate = database.add_boilerplate(
            name=boilerplate_name,
            docopt_string=docopt_string,
            inquirer_answers=json.dumps(inquirer_answers),
        )

        paths = []

        try:
            matches = parse_gitignore(".boilee/.boileeignore", base_dir=".")
        except FileNotFoundError:
            matches = lambda filepath: False

        for root, dirnames, filenames in os.walk("."):
            if len(dirnames) == 0 and len(filenames) == 0 and matches(root):
                paths.append(database.add_path(path=root, is_directory=True))
            else:
                for filename in filenames:
                    filepath = os.path.join(root, filename)

                    if root != "./.boilee" and not matches(filepath):
                        with open(filepath, "rb") as file:
                            paths.append(
                                database.add_path(
                                    path=filepath,
                                    is_directory=False,
                                    content=file.read(),
                                )
                            )

        boilerplate.paths = paths
        database.commit(boilerplate)

        spinner.stop()

        with Halo(text="Generating output file", spinner="dots"):
            os.chdir(run_dir)
            shutil.make_archive(boilerplate_name, "zip", temp_dir)

    click.echo(
        emoji.emojize(
            f':sparkles: :tada:\033[1m boilerplate "{boilerplate_name}" generator successfully! \033[0m:tada: :sparkles:\n',
            use_aliases=True,
        )
    )
