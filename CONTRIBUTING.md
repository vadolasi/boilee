# Contributor Guidelines

First off, thanks for taking the time to contribute!

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Preparation

This project uses the [pipenv packaging tool](https://pipenv.pypa.io/en/latest/). To run this project in a development environment, run the following commands (If pipenv already installed):

```bash
pipenv install --dev
pipenv shell
```

## Commit messages

The commit messages must follow the [Conventional Commits Specification](https://www.conventionalcommits.org/en/v1.0.0/)
